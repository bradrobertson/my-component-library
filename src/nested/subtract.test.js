import subtract from './subtract'

describe("Subtract", () => {
  it("subtracts numbers", () => {
    expect( subtract(2,1) ).toEqual(1)
  })
})
