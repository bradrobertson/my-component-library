import add from './add'

describe("Add", () => {
  it("adds numbers", () => {
    expect( add(1,2) ).toEqual(3)
  })
})
